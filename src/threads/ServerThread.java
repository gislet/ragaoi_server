/****************************************************************************** 
 *
 * ServerThread.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package threads;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerThread extends Thread {

	
	/* Vital server variables. */
	private static ServerSocket _server_socket = null;
	private static Socket _socket = null;
	private static final int PORT_ID = 11113;

	/* Iteration bools. */
	private static boolean _is_alive = true;
	
	/* Game. */	
	private static GameMaster _game_master;
	
	
	
	/* Run() waits for and accepts connecting users to the server, creating a separate thread
	 * for them so the user can interact with the servers functions. */
	public void run() {
				
		/* Setting up server sockets. */
		try {
			_server_socket = new ServerSocket(PORT_ID);
			setupGameMaster();
		} catch(IOException e) {
			e.printStackTrace();
			return;
		}
		
		System.out.println("Server Ready.");

		/* Waits for connecting users. When accepting them the server will set up a
		 * separate thread for them to interact with the server. */
		while(_is_alive) {
			
			
			try {				
				_socket = _server_socket.accept();
				if(_socket != null) {
					Client client = new Client(_socket);
					client.setDaemon(true);
					client.start();
					System.out.println("User Connected: " + _socket.getPort());
				}
            } catch (Exception e) {
            	System.out.println(e);
                break;
            }
		}


		/* Will attempt to close the socket as the thread is stopping. */
		if(!_server_socket.isClosed())
			stopServer();
	}
	
	
	/* */
	private void setupGameMaster() {
		GameMaster gm = new GameMaster();
		gm.setDaemon(true);
		gm.start();
		_game_master = gm;
	}
	
	
	/* Attempts to stop the thread and the server. */
	public void stopServer() {		
		/* Stops the socket from accepting anymore players. */
		_is_alive = false;
		try {
			_server_socket.close();
		} catch(Exception e) {
        	System.out.println(e);
		}
		
		/* Attempts to gracefully disconnect currently connected players. */
		_game_master.close();
	}
}
