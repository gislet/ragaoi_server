/****************************************************************************** 
 *
 * ClientListener.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package threads;
import java.io.IOException;
import java.net.Socket;
import java.util.Properties;
import Websocket.*;
import controller.Json;
import main.Globals;
import model.PlayerInfo;


public class ClientListener extends Thread {

	
	/* Server Variables. */
    private Socket _client_socket;
    private Client _client;
    
    /* */
    private WebSocketInputStream _in_stream;
    
    /* */
    private long _start_time = 0;
    private int _current_ticks = 0;
    private final int MAX_MESSAGE_TICKS = 50;
    private boolean _handshake_success = true;
    
    /* */
    private PlayerInfo _current_status;
	
    
	/* Constructor requires a client socket to start. */
	public ClientListener(Socket p_client_socket, Client p_client) throws Exception {
		_client_socket = p_client_socket;
		_client = p_client;
		setup();		
	}
	
	
	/* Main loop. */
	public void run() {
		
		try {
			if(!handshake())
				_handshake_success = false;
		} catch (IOException e1) {
			_handshake_success = false;
		}
		
		//While loop listens for incomming messages. Will only run if handshake was successful.
		if(_handshake_success) {
			while(_client_socket.isConnected()) {
				try {
					if(!listenForMessages())
						break;
					
					/*if(!isMessagesBlocking())
						break;*/
					sleep(10);
				} catch (IOException | InterruptedException e) {
					System.out.println("ClientListener::run(): " + e);
					break;
				}
			}
		}

		//Attempting to close streams.
		try {
			_in_stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/* */
	private void setup() throws IOException {
		_in_stream = new WebSocketInputStream(_client_socket.getInputStream());
		_start_time = System.currentTimeMillis();
	}	
	
	
	/* */
	private boolean listenForMessages() throws IOException {
		try {
			WebSocketFrame frame = _in_stream.readFrame();
			
			//Escape this method if the frame is empty.
			if(frame == null)
				return true;
			
			//Connection closed. Escape this method and return false.
			if(frame.isCloseFrame()) {
				System.out.printf("Client(%s): %s\n", _client.getClientID(), "Sent Close Frame");
				return false;
			}
			
			//We received a frame with data.
			if(frame.isBinaryFrame()) {
				
				//We're expecting to receive JSON messages.
				PlayerInfo PI = Json.toObject(frame.getPayloadText(), PlayerInfo.class);
				
				//If it wasn't, then PI should be null.
				if(PI == null) {
					System.out.printf("Client(%s): %s\n", _client.getClientID(), frame.getPayloadText());
				} else {
					if(_current_status != null) {
						PI.size = _current_status.size;
					}
					_current_status = PI;
				}
			}
			
			//We haven't implemented anything to process frames other than these. 
			if(!frame.isCloseFrame() && !frame.isBinaryFrame()) {
				//Print what type of frame it is.
				System.out.printf("Binary(%s), Close(%s), Continuation(%s), Control(%s), Data(%s), Ping(%s), Pong(%s), Text(%s)\n", 
						Globals.toString(frame.isBinaryFrame()), Globals.toString(frame.isCloseFrame()), Globals.toString(frame.isContinuationFrame()), 
						Globals.toString(frame.isControlFrame()), Globals.toString(frame.isDataFrame()), Globals.toString(frame.isPingFrame()), 
						Globals.toString(frame.isPongFrame()), Globals.toString(frame.isTextFrame()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	/* */
	private boolean handshake() throws IOException {
		Properties props = new Properties();
		String return_msg = null;
		
		// Constructing the entire incomming handshake message.
		String line;
		while((line = _in_stream.readLine()) != null && !line.equals("")) {
            String[] q = line.split(": ");
            if(q.length > 1)
            	props.put(q[0], q[1]);
        }
		
		//No point in continuing if we didn't manage to put anything inside of props.
		if(props.size() == 0)
			return false;
		
		//Make sure the correct values have been received.
		if(props.get("Upgrade").equals("websocket") && props.get("Sec-WebSocket-Version").equals("13")) {
			String key = (String) props.get("Sec-WebSocket-Key");
			String return_key = key + "" + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"; // magic key
			try {
				return_key = Globals.getSHA1Hash(return_key);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
			return_msg = "HTTP/1.1 101 Switching Protocols\r\n";
			return_msg +="Upgrade: websocket\r\n";
			return_msg +="Connection: Upgrade\r\n";
			return_msg +="Sec-WebSocket-Accept: "+ return_key + "\r\n\r\n";
			
			//Send the message to our messanger thread.
			_client.setMessage(return_msg);
			
			return true;
		}		
		
		return false;
	}
	
	
	/* */
	private boolean isMessagesBlocking() {
		_current_ticks++;
		long elapsed = (System.currentTimeMillis() - _start_time);
		if(elapsed > 1000) {
			_start_time = System.currentTimeMillis();
			_current_ticks = 0;
		} else if(_current_ticks > MAX_MESSAGE_TICKS) {
			return false;
		}
		
		return true;
	}
	
	
	/* */
	public synchronized PlayerInfo currentStatus() {
		return _current_status;
	}
}
