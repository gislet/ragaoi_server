/****************************************************************************** 
 *
 * ClientListener.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package threads;
import java.net.Socket;

import controller.Json;
import main.Globals;
import model.PlayerInfo;
import model.vec2;


public class Client extends Thread {

	
	/* Client Variables. */
    private Socket _client_socket;
    
    /* Communications. */
    private ClientMessenger _messenger;
    private ClientListener _listener;
    
    /* Client Details. */
    private String _client_name;
    private String _client_ID;
	
    
    
    /* Constructor requires a client socket to start. */
	public Client(Socket p_client_socket) {
		this._client_socket = p_client_socket;
		_client_ID = "" + _client_socket.getPort();
	}
	
	
	/* Main loop. */
	public void run() {
		/* Sets up streams */
		if(setupCommunications()) {
			System.out.println("Client " + _client_ID + " failed to setup.");
			return;
		}
		
		sendInitialMessage();
		
		/* Waiting for the setup to finish. */
		while(_listener.isAlive()) {
			try {
				if(currentStatus() != null) {
					GameMaster.addClient(this);
					break;
				}
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		/* This is the primary run-loop. */
		while(_listener.isAlive()) {
			try {
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			try {
				//TODO only send updated status'
				if(_client_socket.isConnected() && _messenger.isAlive()) {
					_messenger.setMessage(GameMaster.getGameStatus());
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		selfDisconnect();
	}
	
	
	/* */
	private void sendInitialMessage() {
		while(currentStatus() == null) {
			if(!_listener.isAlive())
				return;
			
			try {
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		//
		PlayerInfo PI = new PlayerInfo();
		PI.name = currentStatus().name;
		PI.ID = _client_ID;
		vec2 v = GameMaster.getRandomWorldPosition();
		PI.setPosition(v.x, v.y);
		_messenger.setMessage(Json.toString(PI));
		
		
		while(currentStatus().ID.length() == 0) {
			if(!_listener.isAlive())
				return;			
			
			try {
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/* */
	private boolean setupCommunications() {
		try {
			/* Setting up our listener-thread for the connected client. */
			_listener = new ClientListener(_client_socket, this);
			_listener.setDaemon(true);
			_listener.start();
			
			/* Setting up our messanger-thread for the connected client.  */
			_messenger = new ClientMessenger(_client_socket);
			_messenger.setDaemon(true);
			_messenger.start();
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}  
		
		return false;
	}
	
	
	/* Should be called by the client when they disconnect. */
	private void selfDisconnect() {
		disconnect();		
		GameMaster.playerDisconnected(this);
	}
	
	
	/* When called attempts to disconnect the client safely. */
	public void disconnect() {
		try {
			_messenger.close();
			_client_socket.close();
		} catch(Exception e) {
			System.out.println("ClientListener::disconnectClient(): " + e);			
		}
		
		interrupt();
		System.out.printf("Client %s has been disconnected.\n", _client_ID);
	}
	
	
	/* */
	public synchronized void setMessage(String p_message) {
		_messenger.setMessage(p_message);
	}
	
	/* */
	public synchronized PlayerInfo currentStatus() {
		return _listener.currentStatus();
	}
	
	
	/* */
	public synchronized String getClientID() {
		return _client_ID;
	}
	
	
	/* */
	public synchronized void setClientName(String p_name) {
		_client_name = p_name;
	}
	
	/* */
	public synchronized String getClientName() {
		return _client_name;
	}
}
