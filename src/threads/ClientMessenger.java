/****************************************************************************** 
 *
 * ClientMessanger.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package threads;
import java.io.IOException;
import java.net.Socket;
import Websocket.WebSocketFrame;
import Websocket.WebSocketOutputStream;
import enums.MsgStatus;
import main.Globals;


public class ClientMessenger extends Thread {
	
	
	/* Server Variables. */
    private Socket _client_socket;
    
    /* */
    WebSocketOutputStream _out_stream;
    
    /* */
    private String _message;
    private boolean _is_alive = true;
    private MsgStatus _msg_status = MsgStatus.Handshake;
	
	
    
	/* Constructor requires a client socket to start. */
	public ClientMessenger(Socket p_client_socket) throws Exception {
		this._client_socket = p_client_socket;		
		setup();		
	}
	
	
	/* */
	private void setup() throws IOException  {
		_out_stream = new WebSocketOutputStream(_client_socket.getOutputStream());		
	}
	
	
	/* Main loop. */
	public void run() {
		
		/*  */
		while(_is_alive) {			
			try {
				switch(_msg_status) {
				case Handshake:
					if(gotMessage())
						sendHandshake(getMessage());
					break;
				case Initial:
					if(gotMessage())
						send(getMessage());
					break;
				case Waiting:
					setMessage(GameMaster.getGameStatus());
					
					if(gotMessage())
						send(getMessage());					
					break;
				case Sending: //Do nothing, just wait.
					break;
				}
			} catch(IOException e) {
				System.out.println("ClientMessanger::run(): " + e);
				break;
			}
			

			try {
				sleep(25);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		try {
			_out_stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/* */
	public void close() {
		_is_alive = false;
	}
	
	
	/* */
	private void sendHandshake(String p_handshake) throws IOException {
		_out_stream.write(p_handshake);
		_msg_status = MsgStatus.Initial;
	}
	
	
	/* For sending normal messages to the client. (JSON) */
	private void send(String p_message) throws IOException {
		_msg_status = MsgStatus.Sending;		
		_out_stream.write(WebSocketFrame.createBinaryFrame(Globals.getBytesUTF8(p_message)));
		_msg_status = MsgStatus.Waiting;
	}
	
	
	/* */
	public synchronized void setMessage(String p_message) {
		_message = p_message;
	}
	
	
	/* */
	private synchronized String getMessage() {
		if(_message != null) {
			String message = _message;
			_message = null;
			return message;
		}
		
		return null;
	}
	
	
	/* */
	private synchronized boolean gotMessage() {
		if(_message != null && _message.length() > 0)
			return true;
		
		return false;
	}
}
