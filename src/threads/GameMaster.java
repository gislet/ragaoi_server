/****************************************************************************** 
 *
 * PlayerHandler.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package threads;
import java.util.ArrayList;
import java.util.Random;

import controller.Json;
import model.FoodInfo;
import model.GameStatus;
import model.PlayerInfo;
import model.vec2;


public class GameMaster extends Thread {
	
	
	/* Game */
	private static ArrayList<Client> _player_list;
	private static ArrayList<FoodInfo> _all_food;
	private static String _status = "";
	//private static Queue<PlayerInfo> _player_updates;
	
	/* */
	private boolean is_alive = true;
	private long sudo_clock;
	private final long SLEEP_TIME = 25;
	
	/* World Variables. */
	private final static int WORLD_SIZE_X = 50;
	private final static int WORLD_SIZE_Y = 50;
	private final int MAX_FOOD_COUNT = 100;
	private final float MIN_SIZE_DIFF_PLAYER_COLLISION = .5f;
	
	
	
	/* */
	public GameMaster() {
		_player_list = new ArrayList<Client>();
		sudo_clock = 0;
		System.out.println("GameMaster has started a game.");
	}
	
	
	public void run() {
		while(is_alive) {
			try {
				spawnFood();
				collisionDetection();
				updateGameStatus();
				sleep(SLEEP_TIME);
				sudo_clock += SLEEP_TIME;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/* */
	private void collisionDetection() {
		if(_player_list.size() == 0)
			return;
		
		for(int i = 0; i < _player_list.size(); i++) {
			for(int g = _all_food.size() - 1; g >= 0; g--) {
				float total_size = _player_list.get(i).currentStatus().getRadius() + _all_food.get(g).getRadius();
				float distance = -1;
				try {
					distance = vec2.distance(_player_list.get(i).currentStatus().getPosition(), _all_food.get(g).getPosition());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(total_size >= distance && distance >= 0) {
					_player_list.get(i).currentStatus().size += _all_food.get(g).size;
					_all_food.remove(g);
				}
			}
			
			for(int g = i + 1; g < _player_list.size(); g++) {
				if(_player_list.get(i).currentStatus().size != 0 && _player_list.get(g).currentStatus().size != 0) {
					float size_diff = _player_list.get(i).currentStatus().size - _player_list.get(g).currentStatus().size;
					
					if(size_diff > MIN_SIZE_DIFF_PLAYER_COLLISION || size_diff < -MIN_SIZE_DIFF_PLAYER_COLLISION) {
						float total_size = _player_list.get(i).currentStatus().getRadius() + _player_list.get(g).currentStatus().getRadius();
						float distance = -1;
						
						try {
							distance = vec2.distance(_player_list.get(i).currentStatus().getPosition(), _player_list.get(g).currentStatus().getPosition());
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						if(total_size >= distance && distance >= 0) {							
							if(_player_list.get(i).currentStatus().size > _player_list.get(g).currentStatus().size) {
								_player_list.get(i).currentStatus().size += _player_list.get(g).currentStatus().size;
								_player_list.get(g).currentStatus().size = 0;
							} else {
								_player_list.get(g).currentStatus().size += _player_list.get(i).currentStatus().size;
								_player_list.get(i).currentStatus().size = 0;
							}
						}
					}
				}
			}
		}
	}
	
	
	/* */
	private void spawnFood() {
		if(sudo_clock >= 500) {
			sudo_clock = 0;
		} else {
			return;
		}
			
		
		if(_all_food == null) {
			_all_food = new ArrayList<FoodInfo>();
		}
		
		if(_all_food.size() > MAX_FOOD_COUNT)
			return;
		
		vec2 v = getRandomWorldPosition();
		FoodInfo FI = new FoodInfo(_all_food.size() + "", v.x, v.y);
		_all_food.add(FI);
	}
	
	
	/* */
	private void updateGameStatus() {
		GameStatus GS = new GameStatus();
		GS.players =  getAllPlayers();
		GS.food = getAllFood();
		GS.gameworld_x = WORLD_SIZE_X;
		GS.gameworld_y = WORLD_SIZE_Y;
		
		setGameStatus(Json.toString(GS));
	}
	
	
	/* */
	private PlayerInfo[] getAllPlayers() {
		if(_player_list != null) {
			PlayerInfo[] array = new PlayerInfo[_player_list.size()];
			
			for(int i = 0; i < array.length; i++) {
				array[i] = _player_list.get(i).currentStatus();
			}
			
			return array;
		}
		return new PlayerInfo[0];
	}
	
	
	/* */
	private FoodInfo[] getAllFood() {
		if(_all_food != null) {
			FoodInfo[] array = new FoodInfo[_all_food.size()];
			
			for(int i = 0; i < array.length; i++) {
				array[i] = _all_food.get(i);
			}
			
			return array;
		}
		
		return new FoodInfo[0];
	}
	
	
	/* Attempts to gracefully disconnect all currently connected players. */
	private synchronized void disconnectAllPlayers() {
		for(int i = _player_list.size() - 1; i >= 0; i--) {
			_player_list.get(i).disconnect();
			_player_list.remove(i);
		}
	}
	
	/* */
	public static synchronized void addClient(Client p_client) {
		_player_list.add(p_client);
	}
	
	
	/* Retrieves a list of all currently connected players. */
	public synchronized static ArrayList<Client> getPlayers() {
		return _player_list;
	}
	
	
	/* */
	public static vec2 getRandomWorldPosition() {
		vec2 v = new vec2();
		Random rand = new Random();		
		v.x = (rand.nextFloat() * WORLD_SIZE_X) - ((float)WORLD_SIZE_X/2f);
		v.y = (rand.nextFloat() * WORLD_SIZE_Y) - ((float)WORLD_SIZE_Y/2f);
		return v;
	}
	
	/* */
	/*public synchronized static void queueUpdates(PlayerInfo p_playerinfo) {
		_player_updates.add(p_playerinfo);
	}*/
	
	
	/* */
	/*private synchronized PlayerInfo dequeueUpdates() {		
		return _player_updates != null ? _player_updates.poll() : null;
	}*/
	
	
	/* */
	public synchronized static String getGameStatus() {
		return _status;
	}
	
	
	/* */
	private synchronized static void setGameStatus(String p_status) {
		_status = p_status;
	}
	
	
	/* Called by a disconnected player thread to clean up the cached player list. */
	public synchronized static void playerDisconnected(Client p_client) {
		/* We remove the player with the corrosponding thread ID from our player list. */
		for(int i = 0; i < _player_list.size(); i++) {
			if(_player_list.get(i).getId() == p_client.getId()) {
				_player_list.remove(i);
				break;
			}
		}
	}
	
	
	public void close() {
		is_alive = false;
		disconnectAllPlayers();
	}
}
