/****************************************************************************** 
 *
 * ServerCommands.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package controller;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import model.PlayerInfo;
import threads.Client;
import threads.GameMaster;


public class ServerCommands {
	
	
	/* */
	private BufferedReader _user_input;
	
	/* Server Commands. */	
	private final static String STOP_SERVER = "STOP";
	private final static String HELP = "HELP";
	private final static String RESTART = "RESTART";
	private final static String PLAYERS = "PLAYERS";
	
	

	/* */
	public ServerCommands() {
		_user_input = new BufferedReader(new InputStreamReader(System.in));
	}
	
	
	/* */
	public boolean listen() {
		try {
			return commands(_user_input.readLine());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	/* Behaviour for supplied commands. */
	private static boolean commands(String p_command) {
		p_command = p_command.toUpperCase();
		
		if(p_command.compareTo(STOP_SERVER) == 0) {
			return true;
		}
		
		if(p_command.compareTo(HELP) == 0) {
			help();
			return false;
		}
		
		if(p_command.compareTo(PLAYERS) == 0) {
			printPlayers();
			return false;
		}
		
		if(p_command.compareTo(RESTART) == 0) {
			//TODO
			return false;
		}
		
		System.out.printf("Invalid command (%s). Type 'Help' to view possible commands.\n", p_command);
		return false;
	}
	
	
	/* Prints a commandline report of all candidate. */
	private static void printPlayers() {
		ArrayList<Client> players = GameMaster.getPlayers();
		String general_format = "%-35s\n";
		String player_format = "%-25s%-10s%-10s\n";		
		
		System.out.println("");
		System.out.printf(general_format, "---------== Players ==---------");		
		if(players != null && players.size() > 0) {
			System.out.printf(player_format, "PLAYERNAME-ID", "SIZE", "POSITION");
			for(int i = 0; i < players.size(); i++) {
				PlayerInfo PI = players.get(i).currentStatus();
				System.out.printf(player_format, PI.name + "-#" + PI.ID, PI.size + "", PI.position());
			}
		} else {
			System.out.printf(general_format, "NO PLAYERS CURRENTLY ONLINE");
		}
		System.out.printf(general_format, "---------=============---------");
		System.out.println("");
	}
	
	
	/* Prints a commandline help message. */
	private static void help() {
		System.out.println("---------- HELP ----------");
		System.out.println("Type 'Players' to see a list of all current players and their stats.");
		System.out.println("Type 'Restart' to reset all current players to the default values.");
		System.out.println("Type 'Stop' to shut down the server.");
		System.out.println("--------------------------\n");
	}
}
