/****************************************************************************** 
 *
 * JsonHandler.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package controller;
import com.google.gson.Gson;


public final class Json {
	
	
	/* Given the supplied string, this generic method turns the json data into
	 * the supplied class as an object. */
	public static <T> T toObject(String p_file_data, Class<T> p_class) {
		Gson gson = new Gson();
		
		try {
			return gson.fromJson(p_file_data, p_class);
		} catch(Exception e) {
			return null;
		}				
	}
	
	
	/* Creates a Json string out of a generic object. */
	public static <T> String toString(T p_object) {
		Gson gson = new Gson();
		String data = gson.toJson(p_object);		
		return data;
	}
	
	
	/* Creates a Json string out of a generic object array. */
	public static <T> String toString(T[] p_object) {
		Gson gson = new Gson();
		String data = gson.toJson(p_object);		
		return data;
	}
}
