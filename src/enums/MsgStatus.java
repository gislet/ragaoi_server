package enums;

public enum MsgStatus {
	Handshake,
	Initial,
	Waiting,
	Sending
}