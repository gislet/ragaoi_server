package model;

import java.util.Random;

public class FoodInfo {

	/* Details */
	public String ID;
	
	/* Position */
	public float x;
	public float y;
	
	/* */
	public float size;
	
	
	/* */
	public FoodInfo(String p_key, float p_x, float p_y) {
		try {
			Random rand = new Random();
			ID = rand.nextInt(99999) + "-" + p_key;
		} catch (Exception e) {
		}
		setSize(.1f);
		setPosition(p_x, p_y);
	}
	
	
	/* */
	public FoodInfo(FoodInfo p_image) {
		ID = p_image.ID;
		setPosition(p_image.x, p_image.y);
		setSize(p_image.size);
	}
	
	
	public String position() {
		return String.format("(%s, %s)", x + "", y + "");
	}
	

	public vec2 getPosition() {
		return new vec2(x, y);
	}
	
	public float getRadius() {
		double radi_sqrd = size/Math.PI;
		return (float)Math.sqrt(radi_sqrd);
	}
	
	public void setPosition(float p_x, float p_y) {
		x = p_x < 0 ? p_x + getRadius() : p_x > 0 ? p_x - getRadius() : p_x;
		y = p_y < 0 ? p_y + getRadius() : p_y > 0 ? p_y - getRadius() : p_y;
	}
	
	public void setSize(float p_size) {
		size = p_size;
	}
	
	
	public void setID(String p_ID) {
		ID = p_ID;
	}
	
}
