package model;

public class PlayerInfo {

	/* Details */
	public String ID;
	public String name;
	
	/* Position */
	public float x;
	public float y;
	
	/* */
	public float size;
	
	
	/* */
	public PlayerInfo() {
		ID = "";
		name = "Player";
		setSize(1);
		setPosition(0, 0);
	}
	
	
	/* */
	public PlayerInfo(PlayerInfo p_image) {
		ID = p_image.ID;
		name = p_image.name;
		setSize(p_image.size);
		setPosition(p_image.x, p_image.y);
	}
	
	
	public String position() {
		return String.format("(%s, %s)", x + "", y + "");
	}
	
	public vec2 getPosition() {
		return new vec2(x, y);
	}
	
	public void setPosition(float p_x, float p_y) {
		x = p_x < 0 ? p_x + getRadius() : p_x > 0 ? p_x - getRadius() : p_x;
		y = p_y < 0 ? p_y + getRadius() : p_y > 0 ? p_y - getRadius() : p_y;
	}
	
	public float getRadius() {
		double radi_sqrd = size/Math.PI;
		return (float)Math.sqrt(radi_sqrd);
	}
	
	public void setSize(float p_size) {
		size = p_size;
	}
	
	
	public void setID(String p_ID) {
		ID = p_ID;
	}
}
