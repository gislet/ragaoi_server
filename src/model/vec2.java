package model;

public class vec2 {
	
	public float x;
	public float y;
	
	public vec2() {
		x = 0;
		y = 0;
	}
	
	public vec2(float p_x, float p_y) {
		x = p_x;
		y = p_y;
	}
	
	public vec2(vec2 p_vec) {
		x = p_vec.x;
		y = p_vec.y;
	}
	
	
	public static float distance(vec2 v1, vec2 v2) throws Exception {
		if(v1 == null || v2 == null)
			throw new Exception();
		
		float a = v2.x - v1.x;
		float b = v2.y - v1.y;
		
		a *= a;
		b *= b;
		
		return (float)Math.sqrt((a + b));
	}
	
}
