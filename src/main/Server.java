/****************************************************************************** 
 *
 * Server.java
 *
 * <Description>
 *
 *
 * Written by:
 *	- Gisle Thorsen
 *
 * Dated: September 2017
 *
 *****************************************************************************/


package main;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import controller.ServerCommands;
import threads.ServerThread;


public class Server {

	
	/* Server Variables */
	private static ServerThread _server_thread;
	private static ServerCommands _server_commands;
	
	
	
	
	/* */	
    public static void main(String[] args) throws IOException, ClassNotFoundException {
    	
    	/* Allowing the server to be operated by a user. */
    	_server_commands = new ServerCommands();
    	
    	/* This loops while the server is running. Allowing for separate operations on the server
		 * to take place. */
		do{
			if(_server_thread == null)
				startServer();

			/* Listens for server commands. */
			if(_server_commands.listen())
				break;
		} while(_server_thread.isAlive());

		_server_thread.stopServer();
		System.out.println("System shutting down.");
    }
    
    
    /* Sets up and starts the server. */
	private static void startServer() {
		if(_server_thread != null && _server_thread.isAlive())
			System.out.println("Warning! New ServerThread was started while another was alive!");

		_server_thread = new ServerThread();
		_server_thread.setDaemon(true);
		_server_thread.start();
	}
}